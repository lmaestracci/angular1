import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
  @Input() public panier: string;

  constructor() { }

  ngOnInit(): void {
  }

}
